#import "AbsolutePathPlugin.h"
#if __has_include(<absolute_path/absolute_path-Swift.h>)
#import <absolute_path/absolute_path-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "absolute_path-Swift.h"
#endif

@implementation AbsolutePathPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftAbsolutePathPlugin registerWithRegistrar:registrar];
}
@end
