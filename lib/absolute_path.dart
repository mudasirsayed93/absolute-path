
import 'dart:async';

import 'package:flutter/services.dart';

class AbsolutePath {
  static const MethodChannel _channel = MethodChannel('absolute_path');

  static Future<String?> getAbsolutePath(String uri) async {
    final Map<String, dynamic> params = <String, dynamic>{
      'uri': uri,
    };
    final String? path = await _channel.invokeMethod('getAbsolutePath', params);
    return path;
  }
}
